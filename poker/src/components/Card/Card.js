import React, {Fragment} from 'react';
import './Card.css';

 let Card = props => {
     const suits = {
         'diams': '♦',
         'hearts': '♥',
         'spades': '♠',
         'clubs': '♣'
     };

        return (
            <Fragment>
                <button className="add-btn" onClick={props.addhandler}>Deal cards</button>
                {props.cards.map((card, index) => {
                    return (
                        <div key={index} className={"Card Card-rank-" + card.rank + " Card-" + [card.suit] + " "}>
                            <span style={{textTransform: "capitalize"}} className="Card-rank">{card.rank}</span>
                            <span className="Card-suit">{suits[card.suit]}</span>
                        </div>
                    )
                })}
            </Fragment>
        )
};

export default Card;
